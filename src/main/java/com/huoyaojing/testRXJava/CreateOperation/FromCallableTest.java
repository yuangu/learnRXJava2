package com.huoyaojing.testRXJava.CreateOperation;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

import java.util.concurrent.Callable;

public class FromCallableTest {
    public static void main(String[] args)
    {
        Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 1234;
            }
        }).subscribe(integer -> {
                    System.out.println(    integer);
                });
    }
}
