package com.huoyaojing.testRXJava.CreateOperation;

import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/*
fromArray( ) — 一个数组转换成一个Observable
fromIterable() — 一个Iterable转换成一个Observable
fromFuture() — 一个Future转换成一个Observable
参考:https://mcxiaoke.gitbooks.io/rxdocs/content/operators/From.html
 */

public class FromTest {
    public static void main(String[] args) {
        System.out.println("=======fromArray========");
        Integer[] items = {0, 1, 2, 3, 4, 5};
        Observable.fromArray(items)
                .subscribe(integer -> {
                    System.out.println(integer);
                });


        System.out.println("=======fromIterable========");

        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(1);
        list.add(5);

        Observable.fromIterable(list).subscribe(integer -> {
            System.out.println(integer);
        });
        ;

        System.out.println("=======fromFuture(有问题)========");
        Future<Integer> future = new Future<Integer>() {
            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return false;
            }

            @Override
            public Integer get() throws InterruptedException, ExecutionException {
                return null;
            }

            @Override
            public Integer get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return null;
            }
        };

        Observable.fromFuture(future).subscribe(integer -> {
            System.out.println(integer);
        });
    }
}
