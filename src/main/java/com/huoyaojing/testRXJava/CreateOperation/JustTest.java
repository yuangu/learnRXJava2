package com.huoyaojing.testRXJava.CreateOperation;

/*
just( ) — 将一个或多个对象转换成发射这个或这些对象的一个Observable
参考:https://mcxiaoke.gitbooks.io/rxdocs/content/operators/Just.html
 */

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

public class JustTest {

    public static void main(String[] args)
    {
        System.out.println("====非lambad表示方法 ======");
        //非lambda表示方法
        Observable.just(1, 2, 3).subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                System.out.println(integer);
            }
        });


        System.out.println("====lambad的表示方法 ======");
        //lambda表示方法
        Observable.just(1, 2, 3)
                .subscribe(integer -> {
                    System.out.println(    integer);
                });
    }
}
