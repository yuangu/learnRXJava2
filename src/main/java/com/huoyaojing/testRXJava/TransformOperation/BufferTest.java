package com.huoyaojing.testRXJava.TransformOperation;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

import java.util.concurrent.Callable;



/*
• buffer( )  — 它定期从Observable收集数据到一个集合，然后把这些数据集合打包发射，而不是一次发射一个
参考:https://mcxiaoke.gitbooks.io/rxdocs/content/operators/Buffer.html
 */

public class BufferTest {
    public static void main(String[] args) {
        System.out.println("====只有count====");
        Observable.just(0, 1, 2, 3, 4, 5).buffer(2).subscribe(str -> {
            System.out.println(str.getClass().getTypeName() + ":" + str);
        });

        System.out.println("====count + skip ====");
        Observable.just(0, 1, 2, 3, 4, 5).buffer(2, 2).subscribe(str -> {
            System.out.println(str.getClass().getTypeName() + ":" + str);
        });


    }
}
