package com.huoyaojing.testRXJava.TransformOperation;


/*
• flatMap( )  — 将一个发射数据的Observable变换为多个Observables，然后将它们发射的数据合并后放进一个单独的Observable
• flatMapIterable() — 这个变体成对的打包数据，然后生成Iterable而不是原始数据和生成的Observables
• concatMap() 类似于最简单版本的flatMap，但是它按次序连接而不是合并那些生成的Observables，然后产生自己的数据序列。
• switchMap() 它和flatMap很像，除了一点：当原始Observable发射一个新的数据（Observable）时，它将取消订阅并停止监视产生执之前那个数据的Observable，只监视当前这一个
参考:https://mcxiaoke.gitbooks.io/rxdocs/content/operators/FlatMap.html
 */

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Function;

import java.util.ArrayList;
import java.util.List;


public class FlatMapTest {
    public static void main(String[] args) {

        System.out.println("============flatMap=================");
        Observable.just(1, 2, 3).flatMap(new Function<Integer, Observable<String>>() {
            @Override
            public Observable<String> apply(Integer integer) throws Exception {
                return Observable.create((ObservableOnSubscribe<String>) emitter -> {
                    emitter.onNext("第" + integer + "圆球的第1个棱形任务");
                    if(integer != 1) {
                        // 第2和第3个圆球的第二个任务延时。
                        Thread.sleep(5 * 1000);
                    }
                    emitter.onNext("第" + integer + "圆球的第2个棱形任务");
                    emitter.onComplete();
                });
            }
        }).subscribe(str -> {
            System.out.println(str.getClass().getTypeName() + ":" + str);
        });


        System.out.println("============flatMapIterable=================");
        Observable.just(1, 2, 3).flatMapIterable(new Function<Integer, List<String>>() {
            @Override
            public List<String> apply(Integer integer) throws Exception {
                List ret = new ArrayList();
                ret.add("第" + integer + "圆球的第1个棱形任务");
                ret.add("第" + integer + "圆球的第2个棱形任务");
                return ret;
            }
        }).subscribe(str -> {
            System.out.println(str.getClass().getTypeName() + ":" + str);
        });


        System.out.println("============concatMap=================");
        Observable.just(1, 2, 3).concatMap(new Function<Integer, Observable<String>>() {
            @Override
            public Observable<String> apply(Integer integer) throws Exception {
                return Observable.create((ObservableOnSubscribe<String>) emitter -> {
                    emitter.onNext("第" + integer + "圆球的第1个棱形任务");
                    if(integer != 1) {
                        // 第2和第3个圆球的第二个任务延时。
                        Thread.sleep(5 * 1000);
                    }
                    emitter.onNext("第" + integer + "圆球的第2个棱形任务");
                    emitter.onComplete();
                });
            }
        }).subscribe(str -> {
            System.out.println(str.getClass().getTypeName() + ":" + str);
        });

        System.out.println("============switchMap=================");
        Observable.just(1, 2, 3).switchMap(new Function<Integer, Observable<String>>() {
            @Override
            public Observable<String> apply(Integer integer) throws Exception {
                return Observable.create((ObservableOnSubscribe<String>) emitter -> {
                    emitter.onNext("第" + integer + "圆球的第1个棱形任务");
                    if(integer != 1) {
                        // 第2和第3个圆球的第二个任务延时。
                        Thread.sleep(5 * 1000);
                    }
                    emitter.onNext("第" + integer + "圆球的第2个棱形任务");
                    emitter.onComplete();
                });
            }
        }).subscribe(str -> {
            System.out.println(str.getClass().getTypeName() + ":" + str);
        });
    }
}
