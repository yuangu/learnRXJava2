package com.huoyaojing.testRXJava.TransformOperation;

import io.reactivex.*;
//注意还有一个java.util.function.Function,别弄错了
import io.reactivex.functions.Function;
import java.lang.String;

/*
• map( )  — 对序列的每一项都应用一个函数来变换Observable发射的数据序列
参考:https://mcxiaoke.gitbooks.io/rxdocs/content/operators/Map.html
 */
public class MapTest {
    public static void main(String[] args) {
        Observable.just(0, 1, 2, 3, 4, 5).map(new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return String.valueOf(integer);
            }
        }).subscribe(str -> {
            System.out.println(str.getClass().getTypeName() + ":" + str);
        });
    }
}
